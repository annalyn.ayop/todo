import './App.css';

import React from 'react'
import { v4 as uuidv4 } from 'uuid';
import Form from './components/Form'
import List from './components/List'

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            itemsToShow: "all",
            id: uuidv4(),
            item: '',
            editItem: false,
        }
    }

    handleChange = event => {
        this.setState({
            item: event.target.value
        })
    }

    handleSubmit = event => {
        event.preventDefault()

        const item = {
            id: this.state.id,
            title: this.state.item,
            completed: false
        }

        if (this.state.editItem) {
            const updatedItems = this.state.items.map(obj => {
                if (obj.id == item.id) {
                    obj['title'] = item.title;
                    obj['completed'] = item.completed;
                }
                return obj;
            })
            this.setState({
                items: updatedItems,
                id: uuidv4(),
                item: '',
                editItem: false
            })
        } else {
            const updatedItems = [...this.state.items, item]

            if (this.state.item.length > 0) {
                this.setState({
                    items: updatedItems,
                    id: uuidv4(),
                    item: '',
                    editItem: false
                })
            }
        }

    }

    updateTodosToShow = string => {
        this.setState({
            itemsToShow: string
        });
    };

    handleDoneTask = (id, completed) => {
        const filteredItems = this.state.items.map(item => {
            item.id === id && (item.completed = !item.completed)
            return item
        })

        this.setState({
            items: filteredItems,
        })
    }

    handleDelete = id => {
        const filteredItems = this.state.items.filter(item => item.id !== id)

        this.setState({
            items: filteredItems
        })
    }

    handleEdit = id => {
        const filteredItems = this.state.items.filter(item => item.id !== id)

        const selectedItem = this.state.items.find(item => item.id === id)

        this.setState({
            // items: filteredItems,
            id: id,
            item: selectedItem.title,
            editItem: true
        })

    }

    render() {
        let items = []

        if (this.state.itemsToShow === "all") {
            items = this.state.items;
        } else if (this.state.itemsToShow === "todo") {
            items = this.state.items.filter(item => !item.completed);
        } else if (this.state.itemsToShow === "done") {
            items = this.state.items.filter(item => item.completed);
        }

        return (
            <div className="container">
                <Form
                    item={this.state.item}
                    editItem={this.state.editItem}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                />
                <List
                    items={items}
                    filterDoneTasks={this.filterDoneTasks}
                    handleDelete={this.handleDelete}
                    handleEdit={this.handleEdit}
                    updateTodosToShow={this.updateTodosToShow}
                    handleDoneTask={this.handleDoneTask}

                />
            </div>
        );
    }
}

export default App;