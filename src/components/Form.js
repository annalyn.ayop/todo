import React from 'react'

export default class Form extends React.Component {
    render() {
        const { item, handleChange, handleSubmit, editItem } = this.props;

        return (
            <div>
                <form onSubmit={handleSubmit}>
                    <input
                        type="text"
                        className="form__input"
                        placeholder="New Todo"
                        value={item}
                        onChange={handleChange}
                    />
                    <button
                        type="submit"
                        className={`form__btn ${editItem ? 'form__btn--edit' : 'form__btn--add'} ${editItem}`}
                    >
                        {editItem ? 'Edit task' : 'Add new task'}
                    </button>
                </form>
            </div>
        )
    }
}