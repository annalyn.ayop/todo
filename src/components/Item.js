import React from 'react'

export default class Item extends React.Component {
    render() {
        const { id, title, handleDelete, handleEdit, handleDoneTask, completed } = this.props

        return (
            <li className="todo__item">
                <div>{title}</div>
                <div className="todo__action">
                    <span
                        className={`todo__btn  ${completed ? 'todo--completed' : ''}`}
                        onClick={() => handleDoneTask(id)}
                    >

                        <i className={`far ${completed ? 'fa-check-square' : 'fa-square'} icon--success`}></i> Completed
                    </span>
                    <span
                        className="todo__btn"
                        onClick={handleEdit}
                    >
                        <i className="fas fa-pencil-alt icon--primary"></i> EDIT
                    </span>
                    <span
                        className="todo__btn"
                        onClick={handleDelete}
                    >
                        <i className="fas fa-trash-alt icon--danger"></i> DELETE
                    </span>
                </div>
            </li>
        )
    }
}
