import React from 'react'
import Item from './Item'

export default class List extends React.Component {
    render() {
        const {
            items,
            updateTodosToShow,
            handleDelete,
            handleEdit,
            handleDoneTask,
        } = this.props;

        return (
            <div className="list">
                <h3 className="list__heading">Todo List</h3>

                <nav className="list__nav">
                    <button
                        type="button"
                        className="list__btn"
                        onClick={() => updateTodosToShow("all")}
                    >
                        All
                    </button>
                    <button
                        type="button"
                        className="list__btn"
                        onClick={() => updateTodosToShow("done")}
                    >
                        Done
                    </button>
                    <button
                        type="button"
                        className="list__btn"
                        onClick={() => updateTodosToShow("todo")}
                    >
                        Todo
                    </button>
                </nav>

                {
                    items.length === 0 ? '' :
                        <ul className="todo__list">
                            {
                                items.map(item => {
                                    return (
                                        <Item
                                            key={item.id}
                                            id={item.id}
                                            title={item.title}
                                            completed={item.completed}
                                            handleDelete={() => handleDelete(item.id)}
                                            handleEdit={() => handleEdit(item.id)}
                                            handleDoneTask={handleDoneTask}
                                        />
                                    )
                                })
                            }
                        </ul>
                }
            </div>
        )
    }
}
